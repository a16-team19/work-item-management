package models.workitems;

import models.contracts.Member;
import models.contracts.Story;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;
import models.workitems.common.StoryStatusType;
import models.workitems.common.WorkItemType;


public class StoryImpl extends WorkItemBase implements Story {

    private static final String UNASSIGNED = "Unassigned";
    private static final String ASSIGNEE = "Assignee";
    private static final String STATUS = "Status";
    private static final String PRIORITY = "Priority";
    private static final String SIZE = "Size";

    private PriorityType priority;
    private SizeType size;
    private Member assignee;

    public StoryImpl(String title, String description, PriorityType priority, SizeType size) {
        super(title, description);
        setPriority(priority);
        setSize(size);
        setDefaultStatus();
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public void setPriority(PriorityType priority) {
        this.priority = priority;
        addHistory(PRIORITY, priority);
    }

    @Override
    public SizeType getSize() {
        return size;
    }

    @Override
    public void setSize(SizeType size) {
        this.size = size;
        addHistory(SIZE, size);
    }

    @Override
    public String getAssignee() {
        if (assignee == null) {
            return UNASSIGNED;
        }
        return assignee.getName();
    }

    @Override
    public void setAssignee(Member assignee) {
        this.assignee = assignee;
        addHistory(ASSIGNEE, assignee.getName());
    }

    @Override
    public void setStatus(Enum status) {
        this.status = status;
        addHistory(STATUS, status);
    }

    @Override
    public void unAssign() {
        this.assignee = null;
        addHistory(ASSIGNEE, UNASSIGNED);
    }

    @Override
    public WorkItemType getItemType() {
        return WorkItemType.STORY;
    }

    @Override
    public String toString() {
        return String.format("%s" +
                "Status :%s%n" +
                "Size :%s%n" +
                "Priority: %s", super.toString(), getStatus(), getSize(), getPriority()
        );
    }

    private void setDefaultStatus() {
        this.status = StoryStatusType.NOTDONE;
    }
}
