package models.workitems;

import core.WorkItemManagementRepositoryImpl;
import models.ValidationHelper;
import models.contracts.WorkItem;
import models.workitems.common.WorkItemType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class WorkItemBase implements WorkItem {

    private static final String COMMENT_ERROR = "Comment can not be empty!";
    private static final String TITLE_NULL_ERROR = "The title cannot be null.";
    private static final int MIN_TITLE_LENGTH = 10;
    private static final int MAX_TITLE_LENGTH = 50;
    private static final String TITLE_LENGTH_ERROR = "The title's length cannot be less than %d and more than %d symbols long.";
    private static final String DESCRIPTION_NULL_ERROR = "The description cannot be null.";
    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;
    private static final String DESCRIPTION_LENGTH_ERROR = "The description's length cannot be less than %d and more than %d symbols long.";
    private static final String STRING_FOR_HISTORY = "%s changed to %s";

    Enum status;
    private int id;
    private String title;
    private String description;
    private Map<String, List<String>> comments;
    private List<String> history;

    WorkItemBase(String title, String description) {
        setId();
        setTitle(title);
        setDescription(description);
        comments = new HashMap<>();
        history = new ArrayList<>();
    }

    @Override
    public String toString() {
        return String.format("ID :%d%n" +
                        "Title :%s%n"
                , getId(), getTitle());
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public HashMap<String, List<String>> getComments() {
        return new HashMap<>(comments);
    }

    @Override
    public Enum getStatus() {
        return status;
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public void addComment(String memberName, String comment) {
        ValidationHelper.checkObjectNull(comment, COMMENT_ERROR);
        if (!comments.containsKey(memberName)) {
            comments.put(memberName, new ArrayList<>());
        }
        comments.get(memberName).add(comment);
    }

    public abstract WorkItemType getItemType();

    void addHistory(String string, Object name) {
        String change = String.format(STRING_FOR_HISTORY, string, name);
        history.add(change);
    }

    private void setId() {
        this.id = WorkItemManagementRepositoryImpl.idGenerate;
    }

    private void setTitle(String title) {
        ValidationHelper.checkObjectNull(title, TITLE_NULL_ERROR);
        ValidationHelper.checkStringLength(title, MIN_TITLE_LENGTH, MAX_TITLE_LENGTH, TITLE_LENGTH_ERROR);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelper.checkObjectNull(description, DESCRIPTION_NULL_ERROR);
        ValidationHelper.checkStringLength(description, MIN_DESCRIPTION_LENGTH, MAX_DESCRIPTION_LENGTH, DESCRIPTION_LENGTH_ERROR);
        this.description = description;
    }

}
