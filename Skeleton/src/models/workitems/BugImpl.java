package models.workitems;

import models.ValidationHelper;
import models.contracts.Bug;
import models.contracts.Member;
import models.workitems.common.BugStatusType;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import models.workitems.common.WorkItemType;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends WorkItemBase implements Bug {

    private static final String STEPS_NULL_ERROR = "The steps cannot be null.";
    private static final String UNASSIGNED = "Unassigned";
    private static final String STATUS = "Status";
    private static final String SEVERITY = "Severity";
    private static final String PRIORITY = "Priority";
    private static final String ASSIGNEE = "Assignee";

    private List<String> steps;
    private PriorityType priority;
    private SeverityType severity;
    private Member assignee;

    public BugImpl(String title, String description, List<String> steps, PriorityType priority, SeverityType severity) {
        super(title, description);
        setSteps(steps);
        setPriority(priority);
        setSeverity(severity);
        setDefaultStatus();
    }

    @Override
    public List<String> getSteps() {
        return new ArrayList<>(steps);
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public void setPriority(PriorityType priority) {
        this.priority = priority;
        addHistory(PRIORITY, priority);
    }

    @Override
    public SeverityType getSeverity() {
        return severity;
    }

    @Override
    public void setSeverity(SeverityType severity) {
        this.severity = severity;
        addHistory(SEVERITY, severity);
    }

    @Override
    public String getAssignee() {
        if (assignee == null) {
            return UNASSIGNED;
        }
        return assignee.getName();
    }

    @Override
    public void setAssignee(Member assignee) {
        this.assignee = assignee;
        addHistory(ASSIGNEE, assignee.getName());
    }

    @Override
    public void setStatus(Enum status) {
        this.status = status;
        addHistory(STATUS, status);
    }

    @Override
    public void unAssign() {
        this.assignee = null;
        addHistory(STATUS, UNASSIGNED);
    }

    @Override
    public WorkItemType getItemType() {
        return WorkItemType.BUG;
    }

    @Override
    public String toString() {
        return String.format("%s" +
                "Status :%s%n" +
                "Severity :%s%n" +
                "Priority :%s", super.toString(), getStatus(), getSeverity(), getPriority());
    }

    private void setSteps(List<String> steps) {
        ValidationHelper.checkObjectNull(steps, STEPS_NULL_ERROR);
        this.steps = steps;
    }

    private void setDefaultStatus() {
        this.status = BugStatusType.ACTIVE;
    }
}
