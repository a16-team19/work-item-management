package models.workitems;

import models.ValidationHelper;
import models.contracts.Feedback;
import models.workitems.common.FeedbackStatusType;
import models.workitems.common.WorkItemType;

public class FeedbackImpl extends WorkItemBase implements Feedback {

    private static final String RATING_NEGATIVE_ERROR = "Rating cannot be less than %d and more than %d.";
    private static final String STATUS = "Status";
    private static final String RATING = "Rating";
    private static final int MIN_RATING = 0;
    private static final int MAX_RATING = 10;

    private int rating;

    public FeedbackImpl(String title, String description, int rating) {
        super(title, description);
        setDefaultStatus();
        setRating(rating);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void setRating(int rating) {
        ValidationHelper.checkIntInRange(rating, MIN_RATING, MAX_RATING, RATING_NEGATIVE_ERROR);
        this.rating = rating;
        addHistory(RATING, rating);
    }

    @Override
    public void setStatus(Enum status) {
        this.status = status;
        addHistory(STATUS, status);
    }

    @Override
    public WorkItemType getItemType() {
        return WorkItemType.FEEDBACK;
    }

    @Override
    public String toString() {
        return String.format("%s" +
                "Status :%s%n" +
                "Rating %s", super.toString(), getStatus(), getRating()
        );
    }

    private void setDefaultStatus() {
        this.status = FeedbackStatusType.NEW;
    }
}
