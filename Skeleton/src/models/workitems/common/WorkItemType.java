package models.workitems.common;

import models.ValidationHelper;

public enum WorkItemType {
    BUG,
    STORY,
    FEEDBACK;

    @Override
    public String toString() {
        switch (this){
            case BUG:
                return "Bug";
            case STORY:
                return "Story";
            case FEEDBACK:
                return "Feedback";
            default:
                throw new IllegalArgumentException(ValidationHelper.INVALID_ENUM);
        }
    }
}
