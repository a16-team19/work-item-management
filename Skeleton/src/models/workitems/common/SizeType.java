package models.workitems.common;

import models.ValidationHelper;

public enum SizeType {
    LARGE,
    MEDIUM,
    SMALL;

    @Override
    public String toString() {
        switch (this){
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            default:
                throw new IllegalArgumentException(ValidationHelper.INVALID_ENUM);
        }
    }
}
