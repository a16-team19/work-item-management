package models.workitems.common;

import models.ValidationHelper;

public enum FeedbackStatusType {
    NEW,
    UNSCHEDULED,
    DONE;

    @Override
    public String toString() {
        switch (this){
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException(ValidationHelper.INVALID_ENUM);
        }
    }
}
