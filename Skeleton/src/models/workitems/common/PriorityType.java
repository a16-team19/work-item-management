package models.workitems.common;

import models.ValidationHelper;

public enum PriorityType {
    HIGH,
    MEDIUM,
    LOW;

    @Override
    public String toString() {
        switch (this){
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            default:
                throw new IllegalArgumentException(ValidationHelper.INVALID_ENUM);
        }
    }
}
