package models.workitems.common;

import models.ValidationHelper;

public enum BugStatusType {
    ACTIVE,
    FIXED;

    @Override
    public String toString() {
        switch (this){
            case FIXED:
                return "Fixed";
            case ACTIVE:
                return "Active";
            default:
                throw new IllegalArgumentException(ValidationHelper.INVALID_ENUM);
        }
    }
}
