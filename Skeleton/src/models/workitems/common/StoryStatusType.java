package models.workitems.common;

import models.ValidationHelper;

public enum StoryStatusType {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this){
            case NOTDONE:
                return "Not done";
            case INPROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException(ValidationHelper.INVALID_ENUM);
        }
    }
}
