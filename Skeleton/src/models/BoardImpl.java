package models;

import models.contracts.Board;
import models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 10;
    private static final String NAME_LENGTH_ERROR = String.format(
            "The Board name's length cannot be less than %d and more than %d symbols long.", MIN_NAME_LENGTH, MAX_NAME_LENGTH);
    private static final String STRING_FOR_ADDING_WORKITEM = "Added %s with #%d %s";
    private static final String STRING_FOR_REMOVE_WORKITEM = "Remove %s with #%d %s";

    private String name;
    private List<WorkItem> boardWorkItems;
    private List<String> boardActivityHistory;

    public BoardImpl(String name) {
        setName(name);
        boardWorkItems = new ArrayList<>();
        boardActivityHistory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getBoardWorkItems() {
        return boardWorkItems;
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        boardWorkItems.add(workItem);
        addHistory(STRING_FOR_ADDING_WORKITEM, workItem);
    }

    @Override
    public void removeWorkItem(WorkItem workItem) {
        boardWorkItems.remove(workItem);
        addHistory(STRING_FOR_REMOVE_WORKITEM, workItem);
    }

    @Override
    public List<String> getBoardActivityHistory() {
        return boardActivityHistory;
    }

    @Override
    public String toString() {
        return String.format("Board with name %s%n", getName());
    }

    private void setName(String name) {
        ValidationHelper.checkStringLength(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH, NAME_LENGTH_ERROR);
        this.name = name;
    }

    private void addHistory(String message, WorkItem workItem) {
        String change = String.format(message, workItem.getItemType(), workItem.getId(), workItem.getTitle());
        boardActivityHistory.add(change);
    }
}
