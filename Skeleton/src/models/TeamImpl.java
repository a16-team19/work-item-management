package models;

import models.contracts.Board;
import models.contracts.Member;
import models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private static final String ACTIVITY_MESSAGE = "%s added to team";

    private String name;
    private List<Member> members;
    private List<Board> boards;
    private List<String> activity;

    public TeamImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new ArrayList<>();
        activity = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public List<String> getActivity() {
        return new ArrayList<>(activity);
    }

    @Override
    public void addMember(Member member) {
        members.add(member);
        String name = member.getName();
        addActivity(name);
    }

    @Override
    public void addBoard(Board board) {
        boards.add(board);
        String name = board.getName();
        addActivity(name);
    }

    @Override
    public String toString() {
        return String.format("Team with name %s%n", getName());
    }

    private void addActivity(String name) {
        String change = String.format(ACTIVITY_MESSAGE, name);
        activity.add(change);
    }

    private void setName(String name) {
        this.name = name;
    }
}
