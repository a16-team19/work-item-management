package models.contracts;

import java.util.List;

public interface Board {

    String getName();

    List<WorkItem> getBoardWorkItems();

    List<String> getBoardActivityHistory();

    void addWorkItem(WorkItem workItem);

    void removeWorkItem(WorkItem workItem);
}
