package models.contracts;

import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;

import java.util.List;

public interface Bug extends WorkItem {

    List<String> getSteps() ;

    PriorityType getPriority();

    SeverityType getSeverity();

    String getAssignee() ;

    void setAssignee(Member assignee);

    void unAssign();

    void setPriority(PriorityType priority);

    void setSeverity(SeverityType severity);
}
