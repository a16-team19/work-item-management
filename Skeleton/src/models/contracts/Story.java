package models.contracts;

import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;

public interface Story extends WorkItem {

    PriorityType getPriority();

    SizeType getSize();

    String getAssignee() ;

    void setAssignee(Member assignee);

    void unAssign();

    void setPriority(PriorityType priority);

    void setSize(SizeType size);
}
