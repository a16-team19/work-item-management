package models.contracts;

import models.workitems.common.WorkItemType;

import java.util.HashMap;
import java.util.List;

public interface WorkItem {
    int getId();

    String getTitle();

    String getDescription();

    Enum getStatus();

    void setStatus(Enum status);

    HashMap<String, List<String>> getComments();

    List<String> getHistory();

    void addComment(String memberName,String comment);

    WorkItemType getItemType();

}
