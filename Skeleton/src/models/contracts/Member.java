package models.contracts;

import java.util.List;

public interface Member {

    String getName();

    List<WorkItem> getMemberWorkItems();

    List<String> getMemberActivityHistory();

    void addWorkItem(WorkItem workItem);

    void removeWorkItem(WorkItem workItem);

}
