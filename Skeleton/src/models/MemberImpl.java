package models;

import models.contracts.Member;
import models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {

    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 15;
    private static final String NAME_LENGTH_ERROR = String.format(
            "The name's length cannot be less than %d and more than %d symbols long.", MIN_NAME_LENGTH, MAX_NAME_LENGTH);
    private static final String STRING_FOR_ADDING_WORKITEM = "Added %s with #%d %s";
    private static final String STRING_FOR_REMOVING_WORKITEM = "Removed %s with #%d %s";

    private String name;
    private List<WorkItem> memberWorkItems;
    private List<String> memberActivityHistory;

    public MemberImpl(String name) {
        setName(name);
        memberWorkItems = new ArrayList<>();
        memberActivityHistory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getMemberWorkItems() {
        return new ArrayList<>(memberWorkItems);
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        memberWorkItems.add(workItem);
        addHistory(STRING_FOR_ADDING_WORKITEM,workItem);
    }

    @Override
    public void removeWorkItem(WorkItem workItem) {
        memberWorkItems.remove(workItem);
        addHistory(STRING_FOR_REMOVING_WORKITEM,workItem);
    }

    @Override
    public List<String> getMemberActivityHistory() {
        return memberActivityHistory;
    }

    @Override
    public String toString() {
        return String.format("Member: %s",getName());
    }

    private void addHistory(String message, WorkItem workItem){
        String change=String.format(message,workItem.getItemType(),workItem.getId(),workItem.getTitle());
        memberActivityHistory.add(change);
    }

    private void setName(String name) {
        ValidationHelper.checkStringLength(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH, NAME_LENGTH_ERROR);
        this.name = name;
    }
}




