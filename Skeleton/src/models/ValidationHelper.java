package models;

import java.util.List;

import static commands.constants.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ValidationHelper {

    public static final String INVALID_ENUM = "Invalid enum!";

    private static void checkValueInRange(double value, double min, double max, String errorMessage) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void checkStringLength(String value, int min, int max, String errorMessage) {
        String message = String.format(errorMessage, min, max);
        checkValueInRange(value.length(), min, max, message);
    }

    public static void checkObjectNull(Object value, String errorMessage) {
        if (value == null) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void checkIntInRange(int number, int minValue, int maxValue, String errorMessage) {
        if (number <= minValue || number >= maxValue) {
           String mess= String.format(errorMessage,minValue,maxValue);
            throw new IllegalArgumentException(mess);
        }
    }


    public static void checkNumberOfArguments(List<String> parameters, int correctNumberOfArguments) {
        if (parameters.size() != correctNumberOfArguments) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, correctNumberOfArguments, parameters.size()));
        }
    }
}

