package core;

import core.contracts.WorkItemManagementRepository;
import models.contracts.Board;
import models.contracts.Member;
import models.contracts.Team;
import models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkItemManagementRepositoryImpl implements WorkItemManagementRepository {

    public static int idGenerate;
    private Map<Integer, WorkItem> workItems;
    private Map<String, Member> members;
    private List<Team> teams;
    private List<Board> boards;

    public WorkItemManagementRepositoryImpl() {
        workItems = new HashMap<>();
        members = new HashMap<>();
        teams = new ArrayList<>();
        boards = new ArrayList<>();
    }

    @Override
    public Map<Integer, WorkItem> getWorkItems() {
        return workItems;
    }

    @Override
    public void addWorkItem(Integer id, WorkItem workItem) {
        workItems.put(id, workItem);
        idGenerate = workItems.size();
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public void addTeam(Team team) {
        teams.add(team);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void addBoard(Board board) {
        boards.add(board);
    }

    @Override
    public Map<String, Member> getMembers() {
        return new HashMap<>(members);
    }

    @Override
    public void addMember(String name, Member member) {
        members.put(name, member);
    }

}
