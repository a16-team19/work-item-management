package core.contracts;

import models.contracts.*;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import models.workitems.common.SizeType;

import java.util.List;

public interface WorkItemManagementFactory {
  Bug createBug(String title, String description, List<String> steps, PriorityType priority, SeverityType severity);

  Feedback createFeedback (String title, String description, int rating);

  Story createStory (String title, String description, PriorityType priority, SizeType size);

  Team createTeam(String name);

  Member createMember (String name);

  Board createBoard(String name);
}
