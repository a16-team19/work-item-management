package core.contracts;

import models.contracts.Board;
import models.contracts.Member;
import models.contracts.Team;
import models.contracts.WorkItem;

import java.util.List;
import java.util.Map;

public interface WorkItemManagementRepository {

    Map<Integer,WorkItem> getWorkItems();

    void addWorkItem(Integer id, WorkItem workItem);

    List<Team> getTeams();

    void addTeam (Team team);

    List<Board> getBoards();

    void addBoard (Board board);

    Map<String, Member> getMembers();

    void addMember(String name, Member member);
}
