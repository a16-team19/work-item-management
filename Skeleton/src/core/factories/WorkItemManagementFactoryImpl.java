package core.factories;

import core.contracts.WorkItemManagementFactory;
import models.BoardImpl;
import models.MemberImpl;
import models.TeamImpl;
import models.contracts.*;
import models.workitems.BugImpl;
import models.workitems.FeedbackImpl;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import models.workitems.common.SizeType;

import java.util.List;

public class WorkItemManagementFactoryImpl implements WorkItemManagementFactory {

    @Override
    public Bug createBug(String title, String description, List<String> steps, PriorityType priority, SeverityType severity) {
        return new BugImpl(title, description, steps, priority, severity);
    }

    @Override
    public Feedback createFeedback(String title, String description, int rating) {
        return new FeedbackImpl(title, description, rating);
    }

    @Override
    public Story createStory(String title, String description, PriorityType priority, SizeType size) {
        return new StoryImpl(title, description, priority, size);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

}
