package core.factories;

import commands.add.AddComment;
import commands.add.AddMemberInTeam;
import commands.change.*;
import commands.contracts.Command;
import commands.creating.*;
import commands.enums.CommandType;
import commands.listing.*;
import core.contracts.CommandFactory;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;

import static commands.constants.CommandConstants.INVALID_COMMAND;

public class CommandFactoryImpl implements CommandFactory {

    @Override
    public Command createCommand(String commandTypeAsString, WorkItemManagementFactory workItemManagementFactory, WorkItemManagementRepository workItemManagementRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType) {
            case CREATEBUG:
                return new CreateBug(workItemManagementRepository, workItemManagementFactory);
            case CREATESTORY:
                return new CreateStory(workItemManagementRepository, workItemManagementFactory);
            case CREATEFEEDBACK:
                return new CreateFeedback(workItemManagementRepository, workItemManagementFactory);
            case CREATEMEMBER:
                return new CreateMember(workItemManagementRepository, workItemManagementFactory);
            case CREATETEAM:
                return new CreateTeam(workItemManagementRepository, workItemManagementFactory);
            case CREATEBOARD:
                return new CreateBoard(workItemManagementRepository, workItemManagementFactory);
            case SHOWTEAMBOARDS:
                return new ShowTeamBoards(workItemManagementRepository);
            case SHOWMEMBERS:
                return new ShowMembers(workItemManagementRepository);
            case SHOWTEAMS:
                return new ShowTeams(workItemManagementRepository);
            case SHOWTEAMMEMBERS:
                return new ShowTeamMembers(workItemManagementRepository);
            case LISTWORKITEMS:
                return new ListWorkItems(workItemManagementRepository);
            case FILTERSTATUS:
                return new FilterStatus(workItemManagementRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivity(workItemManagementRepository);
            case ADDMEMBER:
                return new AddMemberInTeam(workItemManagementRepository);
            case CHANGEPRIORITY:
                return new ChangePriority(workItemManagementRepository);
            case CHANGESEVERITY:
                return new ChangeSeverity(workItemManagementRepository);
            case CHANGESTATUS:
                return new ChangeStatus(workItemManagementRepository);
            case CHANGESIZE:
                return new ChangeSize(workItemManagementRepository);
            case CHANGERATING:
                return new ChangeRating(workItemManagementRepository);
            case ASSIGN:
                return new Assign(workItemManagementRepository);
            case UNASSIGN:
                return new UnAssign(workItemManagementRepository);
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivity(workItemManagementRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(workItemManagementRepository);
            case ADDCOMMENT:
                return new AddComment(workItemManagementRepository);
            case FILTERASSIGN:
                return new FilterAssignMember(workItemManagementRepository);
            case SORTWORKITEMS:
                return new SortWorkItems(workItemManagementRepository);
            case FILTERSTATUSASSIGNEE:
                return new FilterStastusAndAssignMember(workItemManagementRepository);
            case LISTCOMMENTS:
                return new ListComments(workItemManagementRepository);
        }
        throw new IllegalArgumentException(INVALID_COMMAND);
    }
}


