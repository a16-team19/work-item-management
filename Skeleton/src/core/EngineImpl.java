package core;

import commands.contracts.Command;
import core.contracts.*;
import core.factories.CommandFactoryImpl;
import core.factories.WorkItemManagementFactoryImpl;
import core.providers.CommandParserImpl;
import core.providers.ConsoleReader;
import core.providers.ConsoleWriter;

import java.util.List;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private WorkItemManagementFactory workItemManagementFactory;
    private WorkItemManagementRepository workItemManagementRepository;
    private CommandParser commandParser;
    private Writer writer;
    private Reader reader;
    private CommandFactory commandFactory;

    public EngineImpl() {
        workItemManagementFactory = new WorkItemManagementFactoryImpl();
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        commandParser = new CommandParserImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
        commandFactory = new CommandFactoryImpl();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, workItemManagementFactory, workItemManagementRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
