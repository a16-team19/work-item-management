package core.providers;

import core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.List;

public class CommandParserImpl implements CommandParser {
    public String parseCommand(String fullCommand) {
        return fullCommand.split(" ")[0];
    }

    public List<String> parseParameters(String fullCommand) {
        String [] commandParametersSplit = fullCommand.split(" ",2);
        if  (commandParametersSplit.length == 1 || commandParametersSplit[1].equals("")){
            return new ArrayList<>();
        }
        String[] commandParts = commandParametersSplit[1].split("_");
        ArrayList<String> parameters = new ArrayList<>();
        for (String commandPart : commandParts) {
            parameters.add(commandPart.trim());
        }
        return parameters;
    }
}
