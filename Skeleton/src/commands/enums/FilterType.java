package commands.enums;

public enum FilterType {
    ALL,
    BUG,
    STORY,
    FEEDBACK
}
