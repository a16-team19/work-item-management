package commands.enums;

public enum SortType {
    TITLE,
    PRIORITY,
    SEVERITY,
    SIZE,
    RATING
}
