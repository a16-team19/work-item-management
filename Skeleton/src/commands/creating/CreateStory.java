package commands.creating;

import commands.base.CreateCommandBase;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Board;
import models.contracts.Story;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class CreateStory extends CreateCommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 6;

    public CreateStory(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository, workItemManagementFactory);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        String title = parameters.get(2);
        String description = parameters.get(3);
        PriorityType priority = PriorityType.valueOf(parameters.get(4).toUpperCase());
        SizeType size = SizeType.valueOf(parameters.get(5).toUpperCase());

        String isValid = validateCreatingWorkitem(teamName, boardName);
        if (isValid.equals(VALID)) {
            return createStory(teamName, boardName, title, description, priority, size);
        }
        return isValid;
    }

    private String createStory(String teamName, String boardName, String title, String description, PriorityType priority, SizeType size) {
        Board board = getBoardFromTeam(boardName, teamName);
        Story story = getWorkItemManagementFactory().createStory(title, description, priority, size);
        getWorkItemManagementRepository().addWorkItem(story.getId(), story);
        board.addWorkItem(story);

        return String.format(ADD_WORKITEM_TO_BOARD_SUCCESS_MESSAGE,
                story.getItemType(),
                story.getId(),
                story.getTitle(),
                teamName,
                boardName);
    }

}
