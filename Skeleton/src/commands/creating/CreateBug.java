package commands.creating;

import commands.base.CreateCommandBase;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Board;
import models.contracts.Bug;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class CreateBug extends CreateCommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 7;

    public CreateBug(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository, workItemManagementFactory);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        String title = parameters.get(2);
        String description = parameters.get(3);
        List<String> steps = Arrays.stream(parameters.get(4).trim().split(",")).collect(Collectors.toList());
        PriorityType priority = PriorityType.valueOf(parameters.get(5).toUpperCase());
        SeverityType severity = SeverityType.valueOf(parameters.get(6).toUpperCase());

        String isValid = validateCreatingWorkitem(teamName, boardName);
        if (isValid.equals(VALID)) {
            return createBugInBoard(teamName, boardName, title, description, steps, priority, severity);
        }
        return isValid;
    }

    private String createBugInBoard(String teamName, String boardName, String title, String description, List<String> steps, PriorityType priority, SeverityType severity) {
        Board board = getBoardFromTeam(boardName, teamName);
        Bug bug = getWorkItemManagementFactory().createBug(title, description, steps, priority, severity);
        getWorkItemManagementRepository().addWorkItem(bug.getId(), bug);
        board.addWorkItem(bug);

        return String.format(ADD_WORKITEM_TO_BOARD_SUCCESS_MESSAGE,
                bug.getItemType(),
                bug.getId(),
                bug.getTitle(),
                teamName,
                boardName);
    }


}
