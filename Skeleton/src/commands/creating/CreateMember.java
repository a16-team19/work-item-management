package commands.creating;

import commands.base.CreateCommandBase;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Member;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class CreateMember extends CreateCommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String MEMBER_EXIST = "Member %s already exist!";

    public CreateMember(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository, workItemManagementFactory);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String name = parameters.get(0);

        return createMember(name);
    }

    private String createMember(String name) {
        if (doesMemberExist(name)) {
            return String.format(MEMBER_EXIST, name);
        }
        Member member = getWorkItemManagementFactory().createMember(name);
        getWorkItemManagementRepository().addMember(member.getName(), member);

        return String.format(MEMBER_CREATED_SUCCESS_MESSAGE,
                member.getName());
    }
}
