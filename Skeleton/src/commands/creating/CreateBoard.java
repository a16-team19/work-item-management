package commands.creating;

import commands.base.CreateCommandBase;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Board;
import models.contracts.Team;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class CreateBoard extends CreateCommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public CreateBoard(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository, workItemManagementFactory);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);

        return createBoard(teamName, boardName);
    }

    private String createBoard(String teamName, String boardName) {
        Team team = getTeam(teamName);

        if (doesTeamExist(teamName)) {
            if (doesBoardExistTeam(teamName, boardName)) {
                return String.format(BOARD_EXIST_IN_TEAM, boardName, teamName);
            }
            Board board = getWorkItemManagementFactory().createBoard(boardName);
            team.addBoard(board);
            getWorkItemManagementRepository().addBoard(board);
        } else {
            return String.format(INVALID_TEAM_NAME, teamName);
        }

        return String.format(BOARD_CREATED_SUCCESS_MESSAGE,
                boardName,
                teamName);
    }
}

