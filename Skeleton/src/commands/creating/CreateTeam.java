package commands.creating;

import commands.base.CreateCommandBase;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Team;

import java.util.List;

import static commands.constants.CommandConstants.TEAM_ALREADY_EXISTS;
import static commands.constants.CommandConstants.TEAM_CREATED_SUCCESS_MESSAGE;

public class CreateTeam extends CreateCommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public CreateTeam(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository, workItemManagementFactory);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String teamName = parameters.get(0);
        if (doesTeamExist(teamName)) {
            return String.format(TEAM_ALREADY_EXISTS, teamName);
        } else {
            return createTeam(teamName);
        }
    }

    private String createTeam(String teamName) {
        Team team = getWorkItemManagementFactory().createTeam(teamName);
        getWorkItemManagementRepository().addTeam(team);

        return String.format(TEAM_CREATED_SUCCESS_MESSAGE,
                team.getName());
    }
}
