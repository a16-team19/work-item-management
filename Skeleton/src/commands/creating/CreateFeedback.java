package commands.creating;

import commands.base.CreateCommandBase;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Board;
import models.contracts.Feedback;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class CreateFeedback extends CreateCommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    public CreateFeedback(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository, workItemManagementFactory);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        String title = parameters.get(2);
        String description = parameters.get(3);
        int rating = Integer.parseInt(parameters.get(4));

        String isValid = validateCreatingWorkitem(teamName, boardName);
        if (isValid.equals(VALID)) {
            return createFeedbackInBoard(teamName, boardName, title, description, rating);
        }
        return isValid;
    }

    private String createFeedbackInBoard(String teamName, String boardName, String title, String description, int rating) {
        Board board = getBoardFromTeam(boardName, teamName);
        Feedback feedback = getWorkItemManagementFactory().createFeedback(title, description, rating);
        getWorkItemManagementRepository().addWorkItem(feedback.getId(), feedback);
        board.addWorkItem(feedback);


        return String.format(ADD_WORKITEM_TO_BOARD_SUCCESS_MESSAGE,
                feedback.getItemType(),
                feedback.getId(),
                feedback.getTitle(),
                teamName,
                boardName);
    }

}
