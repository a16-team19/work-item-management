package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.WorkItem;

import java.util.List;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class ListComments extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ListComments(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));

        return listComments(workItemId);
    }

    private String listComments(int workItemId) {

        WorkItem workItem = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(workItem, String.format(INVALID_ITEM_ID, workItemId));

        List<String> comments = workItem.getComments().keySet()
                .stream()
                .map(key -> key + COLON + workItem.getComments().get(key)
                        .stream()
                        .map(String::toString)
                        .collect(Collectors.joining(System.lineSeparator())))
                .collect(Collectors.toList());

        if (comments.isEmpty()) {
           return NO_COMMENTS;
        }
        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), comments).trim();
    }
}
