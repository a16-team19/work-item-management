package commands.listing;

import commands.base.CommandBase;
import commands.enums.FilterType;
import commands.enums.SortType;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Bug;
import models.contracts.Feedback;
import models.contracts.Story;
import models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static commands.constants.CommandConstants.*;

public class SortWorkItems extends CommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String WORKITEM_EMPTY = "There is not a single work item here!";

    public SortWorkItems(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        FilterType filterType = FilterType.valueOf(parameters.get(0).toUpperCase().trim());
        SortType sortType = SortType.valueOf(parameters.get(1).toUpperCase().trim());

        return sortWorkItems(filterType, sortType);
    }

    private String sortWorkItems(FilterType filterType, SortType sortType) {
        List<WorkItem> workItemList = new ArrayList<>(getWorkItemManagementRepository().getWorkItems().values());
        List<String> sortedWorkItems = new ArrayList<>();

        switch (filterType) {
            case ALL:
                if (sortType == SortType.TITLE) {
                    sortedWorkItems = getSortedItems(workItemList, Comparator.comparing(WorkItem::getTitle));
                    break;
                }
                throw new IllegalArgumentException(INVALID_COMMAND);
            case BUG:
                List<Bug> bugList = getFilteredItemsStream(filterType, workItemList)
                        .map(workItem -> (Bug) workItem)
                        .collect(Collectors.toList());
                if (sortType == SortType.TITLE) {
                    sortedWorkItems = getSortedItems(bugList, Comparator.comparing(Bug::getTitle));
                    break;
                } else if (sortType == SortType.SEVERITY) {
                    sortedWorkItems = getSortedItems(bugList, Comparator.comparing(Bug::getSeverity));
                    break;
                } else if (sortType == SortType.PRIORITY) {
                    sortedWorkItems = getSortedItems(bugList, Comparator.comparing(Bug::getPriority));
                    break;
                }
                throw new IllegalArgumentException(INVALID_COMMAND);
            case STORY:
                List<Story> storyList = getFilteredItemsStream(filterType, workItemList)
                        .map(workItem -> (Story) workItem)
                        .collect(Collectors.toList());
                if (sortType == SortType.TITLE) {
                    sortedWorkItems = getSortedItems(storyList, Comparator.comparing(Story::getTitle));
                    break;
                } else if (sortType == SortType.SIZE) {
                    sortedWorkItems = getSortedItems(storyList, Comparator.comparing(Story::getSize));
                    break;
                } else if (sortType == SortType.PRIORITY) {
                    sortedWorkItems = getSortedItems(storyList, Comparator.comparing(Story::getPriority));
                    break;
                }
                throw new IllegalArgumentException(INVALID_COMMAND);
            case FEEDBACK:
                List<Feedback> feedbackList = getFilteredItemsStream(filterType, workItemList)
                        .map(workItem -> (Feedback) workItem)
                        .collect(Collectors.toList());
                if (sortType == SortType.TITLE) {
                    sortedWorkItems = getSortedItems(feedbackList, Comparator.comparing(Feedback::getTitle));
                    break;
                } else if (sortType == SortType.RATING) {
                    sortedWorkItems = getSortedItems(feedbackList, Comparator.comparing(Feedback::getRating).reversed());
                    break;
                }
                throw new IllegalArgumentException(INVALID_COMMAND);
        }

        if (sortedWorkItems.isEmpty()) {
            return WORKITEM_EMPTY;
        }

        return WORKITEM_PRINT  + System.lineSeparator() + NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), sortedWorkItems).trim();
    }

    private Stream<WorkItem> getFilteredItemsStream(FilterType filterType, List<WorkItem> workItemList) {
        return workItemList
                .stream()
                .filter(o -> o.getItemType().toString().equalsIgnoreCase(filterType.toString()));
    }

    private List<String> getSortedItems(List<? extends WorkItem> workItemList, Comparator<? extends WorkItem> comparing) {
        return workItemList
                .stream()
                .sorted((Comparator<? super WorkItem>) comparing)
                .map(Objects::toString)
                .collect(Collectors.toList());
    }


}
