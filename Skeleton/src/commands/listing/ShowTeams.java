package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Team;

import java.util.List;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class ShowTeams extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;

    public ShowTeams(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        return listTeams();
    }

    private String listTeams() {

        List<String> teamNames = getWorkItemManagementRepository().getTeams()
                .stream()
                .map(Team::getName)
                .collect(Collectors.toList());

        if (teamNames.isEmpty()) {
            return TEAMS_EMPTY;
        }

        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), teamNames).trim();
    }
}


