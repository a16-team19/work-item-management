package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Member;

import java.util.List;

import static commands.constants.CommandConstants.*;


public class ShowPersonActivity extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowPersonActivity(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String memberName = parameters.get(0);

        return showPersonActivity(memberName);
    }


    private String showPersonActivity(String memberName) {

        Member member = getMember(memberName);

        ValidationHelper.checkObjectNull(member,String.format(INVALID_MEMBER_NAME,memberName));

        List<String> personActivity = member.getMemberActivityHistory();

        if (personActivity.isEmpty()) {
            return String.format(PERSON_ACTIVITIES_EMPTY,member.getName());
        }
        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), personActivity).trim();
    }
}
