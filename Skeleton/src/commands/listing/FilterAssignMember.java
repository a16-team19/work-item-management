package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Member;

import java.util.List;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class FilterAssignMember extends CommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public FilterAssignMember(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        String memberName = parameters.get(0);

        return filterAssigns(memberName);
    }

    private String filterAssigns(String memberName) {

        Member member = getMember(memberName);

        ValidationHelper.checkObjectNull(member,String.format(INVALID_MEMBER_NAME,memberName));

        List<String> filteredWorkItems = member
                .getMemberWorkItems()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toList());

        if (filteredWorkItems.isEmpty()) {
            return String.format(WORKITEM_LIST_EMPTY, memberName);
        }

        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), filteredWorkItems).trim();
    }
}

