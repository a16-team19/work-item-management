package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Team;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class ShowTeamActivity extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowTeamActivity(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String teamName = parameters.get(0);

        return showTeamActivity(teamName);
    }

    private String showTeamActivity(String teamName) {

        Team team = getTeam(teamName);

        ValidationHelper.checkObjectNull(team, String.format(INVALID_TEAM_NAME, teamName));

        List<String> teamActivity = team.getActivity();

        if (teamActivity.isEmpty()) {
            return TEAM_ACTIVITIES_EMPTY;
        }

        return  String.format(TEAM,team.getName())
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), teamActivity).trim();
    }
}
