package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Team;

import java.util.List;

import static commands.constants.CommandConstants.*;


public class ShowBoardActivity extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public ShowBoardActivity(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String teamName = parameters.get(0);
        String boardName = parameters.get(1);

        return showPersonActivity(teamName, boardName);
    }

    private String showPersonActivity(String teamName, String boardName) {

        Team team = getTeam(teamName);
        ValidationHelper.checkObjectNull(team,WRONG_TEAM_NAME);

        ValidationHelper.checkObjectNull(getBoardFromTeam(boardName, teamName),WRONG_BOARD_NAME);

        List<String> boardActivity = getBoardFromTeam(boardName, teamName).getBoardActivityHistory();

        if (boardActivity.isEmpty()) {
            return BOARD_ACTIVITIES_EMPTY;
        }
        return String.format(TEAM,team.getName())
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), boardActivity).trim();
    }
}
