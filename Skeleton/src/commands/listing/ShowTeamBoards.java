package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Board;
import models.contracts.Team;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class ShowTeamBoards extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowTeamBoards(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String teamName = parameters.get(0);

        return listTeamBoards(teamName);
    }

    private String listTeamBoards(String teamName) {
        Team team = getTeam(teamName);
        ValidationHelper.checkObjectNull(team, String.format(INVALID_TEAM_NAME, teamName));

        List<Board> boards = team.getBoards();

        if (boards.isEmpty()) {
            return String.format(ZERO_BOARDS_TEAM, teamName);
        }

        List<String> foundBoards = boards
                .stream()
                .map(Objects::toString)
                .collect(Collectors.toList());

        return String.format(TEAM, teamName) + JOIN_DELIMITER + System.lineSeparator()
                + String.join(JOIN_DELIMITER + System.lineSeparator(), foundBoards);
    }
}


