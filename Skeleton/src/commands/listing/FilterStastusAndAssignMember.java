package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Member;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class FilterStastusAndAssignMember extends CommandBase {

    private static final String WORKITEM_STATUS_ASSIGNE_EMPTY = "There is not a single work item here with status %s and assigned to %s!";

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public FilterStastusAndAssignMember(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String status = parameters.get(0).toUpperCase();
        String assigneeName = parameters.get(1);

        return filterStatus(status, assigneeName);
    }

    private String filterStatus(String status, String assigneName) {

        Member member = getMember(assigneName);

        ValidationHelper.checkObjectNull(member,String.format(INVALID_MEMBER_NAME,assigneName));

        List<String> filteredWorkItems = member.getMemberWorkItems().stream()
                .filter(workItem -> workItem.getStatus().toString().equalsIgnoreCase(status))
                .map(Objects::toString)
                .collect(Collectors.toList());

        if (filteredWorkItems.isEmpty()) {
            return String.format(WORKITEM_STATUS_ASSIGNE_EMPTY, status, assigneName);
        }

        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), filteredWorkItems).trim();
    }

}
