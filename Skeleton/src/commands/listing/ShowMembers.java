package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;

public class ShowMembers extends CommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;

    public ShowMembers(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        return listMembers();
    }

    private String listMembers() {
        List<String> memberNames = new ArrayList<>(getWorkItemManagementRepository().getMembers().keySet());
        if (memberNames.isEmpty()) {
            return MEMBERS_EMPTY;
        }
        return MEMBERS_PRINT + System.lineSeparator() + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), memberNames).trim();
    }
}
