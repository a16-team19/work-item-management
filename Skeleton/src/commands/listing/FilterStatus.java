package commands.listing;

import com.sun.xml.internal.ws.util.StringUtils;
import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class FilterStatus extends CommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public FilterStatus(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String status = parameters.get(0).toUpperCase();

        return filterStatus(status);
    }

    private String filterStatus(String status) {

        List<String> filteredWorkItems = getWorkItemManagementRepository()
                .getWorkItems()
                .values()
                .stream()
                .filter(workItem -> workItem.getStatus().toString().equalsIgnoreCase(status))
                .map(Objects::toString)
                .collect(Collectors.toList());

        if (filteredWorkItems.isEmpty()) {
            return String.format(WORKITEM_STATUS_EMPTY, StringUtils.capitalize(status.toLowerCase()));
        }

        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), filteredWorkItems).trim();
    }
}
