package commands.listing;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Member;
import models.contracts.Team;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class ShowTeamMembers extends CommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowTeamMembers(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String teamName = parameters.get(0);

        return listMembersTeam(teamName);
    }

    private String listMembersTeam(String teamName) {
        Team team = getTeam(teamName);
        ValidationHelper.checkObjectNull(team, String.format(INVALID_TEAM_NAME, teamName));

        List<Member> teamMembers = team.getMembers();

        if (teamMembers.isEmpty()) {
            return String.format(ZERO_MEMBERS_TEAM, teamName);
        }

        List<String> listMembers = teamMembers
                .stream()
                .map(Objects::toString)
                .collect(Collectors.toList());

        return String.format(TEAM, teamName) + JOIN_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), listMembers).trim();
    }
}



