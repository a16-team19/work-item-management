package commands.listing;

import commands.base.CommandBase;
import commands.enums.FilterType;
import core.contracts.WorkItemManagementRepository;
import models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static commands.constants.CommandConstants.*;

public class ListWorkItems extends CommandBase {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ListWorkItems(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {
        FilterType filterType = FilterType.valueOf(parameters.get(0).toUpperCase());

        return filterWorkItems(filterType);
    }

    private String filterWorkItems(FilterType filterType) {
        List<WorkItem> workItems = new ArrayList<>(getWorkItemManagementRepository().getWorkItems().values());
        List<String> filteredWorkItems;

        if (filterType == FilterType.ALL) {
            filteredWorkItems = workItems.stream()
                    .map(Objects::toString)
                    .collect(Collectors.toList());
        } else {
            filteredWorkItems = workItems.stream()
                    .filter(workItem -> workItem.getItemType().toString().equalsIgnoreCase(filterType.toString()))
                    .map(Objects::toString)
                    .collect(Collectors.toList());
        }

        if (filteredWorkItems.isEmpty()) {
            return WORK_ITEM_EMPTY;
        }

        return NEW_COMMAND_DELIMITER + System.lineSeparator()
                + String.join(System.lineSeparator() + JOIN_DELIMITER + System.lineSeparator(), filteredWorkItems).trim();
    }
}
