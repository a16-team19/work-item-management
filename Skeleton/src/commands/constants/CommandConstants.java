package commands.constants;

public class CommandConstants {

    // Error messages
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d!";
    public static final String INVALID_COMMAND = "Invalid command!";
    public static final String INVALID_ITEM = "Invalid command for item:#%d %s";
    public static final String INVALID_TEAM_NAME = "Team %s does not exist!";
    public static final String INVALID_MEMBER_NAME = "Member %s does not exist!";
    public static final String INVALID_BOARD_NAME = "Board %s does not exist!";
    public static final String INVALID_ITEM_ID = "Item %d does not exist!";
    public static final String TEAM_ALREADY_EXISTS = "Team %s already exists!";
    public static final String ZERO_BOARDS_TEAM = "There are no boards for team %s!";
    public static final String ZERO_MEMBERS_TEAM = "There are no members for team %s!";
    public static final String NO_COMMENTS = "No comments in this work item!";
    public static final String BOARD_EXIST_IN_TEAM = "Board %s already exist in %s team!";
    public static final String PERSON_ACTIVITIES_EMPTY = "%s activities are empty!";
    public static final String BOARD_ACTIVITIES_EMPTY = "Board activities are empty!";
    public static final String TEAM_ACTIVITIES_EMPTY = "Team activities are empty!";
    public static final String WRONG_TEAM_NAME = "Wrong team name!";
    public static final String WRONG_BOARD_NAME = "Wrong board name!";
    public static final String ALREADY_ASSIGNED = "Work item id already assigned to: %s ";
    public static final String MEMBERS_EMPTY = "There is not a single member here!";
    public static final String WORKITEM_LIST_EMPTY = "There is not a single work item assigned to %s!";
    public static final String TEAMS_EMPTY = "There is not a single team here!";
    public static final String WORK_ITEM_EMPTY = "There is not a single work item here!";
    public static final String WORKITEM_STATUS_EMPTY = "There is not a single work item here with status %s!";
    public static final String MEMBER_NOT_PART_OF_TEAM = "%s is not part of any team!";

    // Success messages
    public static final String PRIORITY_CHANGED_SUCCESS_MESSAGE = "Change priority for ID: %d %s to: %s!";
    public static final String SEVERITY_CHANGED_SUCCESS_MESSAGE = "Change severity for ID: %d %s to: %s!";
    public static final String STATUS_CHANGED_SUCCESS_MESSAGE = "Change status for ID: %d %s to: %s!";
    public static final String MEMBER_CREATED_SUCCESS_MESSAGE = "Member %s created!";
    public static final String TEAM_CREATED_SUCCESS_MESSAGE = "Team %s created!";
    public static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board %s created in team %s!";
    public static final String MEMBER_ADDED_TEAM_SUCCESS_MESSAGE = "Member %s added in team %s!";
    public static final String SIZE_CHANGED_SUCCESS_MESSAGE = "Change size for ID: %d %s to: %s!";
    public static final String RATING_CHANGED_SUCCESS_MESSAGE = "Change rating for ID: %d %s to: %d!";
    public static final String ASSIGN_SUCCESS_MESSAGE = "Assign work item: %d %s to member: %s!";
    public static final String UNASSIGNED_SUCCESS_MESSAGE = "Unassigned work item: %d %s from member: %s!";
    public static final String COMMENT_SUCCESS_MESSAGE = "Added comment :%s to #%d %s from %s";
    public static final String ADD_WORKITEM_TO_BOARD_SUCCESS_MESSAGE = "Added work item :%s #%d %s to %s %s!";



    // Helper
    public static final String JOIN_DELIMITER = "####################";
    public static final String NEW_COMMAND_DELIMITER = "--------------------";
    public static final String COLON = ":\n";
    public static final String VALID = "Valid";
    public static final String TEAM = "Team %s:%n";
    public static final String WORKITEM_PRINT = "Work items:";
    public static final String MEMBERS_PRINT = "Members:";
}
