package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Bug;
import models.contracts.WorkItem;
import models.workitems.common.SeverityType;
import models.workitems.common.WorkItemType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class ChangeSeverity extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public ChangeSeverity(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        SeverityType severityType = SeverityType.valueOf(parameters.get(1).toUpperCase().trim());

        return changeSeverity(workItemId, severityType);
    }


    private String changeSeverity(int workItemId, SeverityType severityType) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        if (item.getItemType().equals(WorkItemType.BUG)) {
            Bug bug = (Bug) item;
            bug.setSeverity(severityType);
        } else {
            return String.format(INVALID_ITEM, item.getId(), item.getTitle());
        }

        return String.format(SEVERITY_CHANGED_SUCCESS_MESSAGE, workItemId, item.getTitle(), severityType.toString());
    }
}

