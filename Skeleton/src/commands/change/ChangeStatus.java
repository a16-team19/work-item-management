package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.WorkItem;
import models.workitems.common.*;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class ChangeStatus extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public ChangeStatus(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        String status = parameters.get(1).toUpperCase().trim();

        return changeSeverity(workItemId, status);
    }


    private String changeSeverity(int workItemId, String status) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        switch (item.getItemType()) {
            case BUG:
                item.setStatus(BugStatusType.valueOf(status));
                break;
            case STORY:
                item.setStatus(StoryStatusType.valueOf(status));
                break;
            case FEEDBACK:
                item.setStatus(FeedbackStatusType.valueOf(status));
                break;
        }

        return String.format(STATUS_CHANGED_SUCCESS_MESSAGE, item.getId(), item.getTitle(), item.getStatus().toString());
    }
}
