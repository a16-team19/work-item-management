package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Bug;
import models.contracts.Member;
import models.contracts.Story;
import models.contracts.WorkItem;
import models.workitems.common.WorkItemType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class UnAssign extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public UnAssign(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        String member = parameters.get(1);

        return unAssign(workItemId, member);
    }


    private String unAssign(int workItemId, String member) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        Member memberToUnAssign = getMember(member);
        ValidationHelper.checkObjectNull(memberToUnAssign, String.format(INVALID_MEMBER_NAME, member));

        memberToUnAssign.removeWorkItem(item);

        if (item.getItemType().equals(WorkItemType.BUG)) {
            Bug bug = (Bug) item;
            bug.unAssign();
        } else if (item.getItemType().equals(WorkItemType.STORY)) {
            Story story = (Story) item;
            story.unAssign();
        } else {
            return String.format(INVALID_ITEM, item.getId(), item.getTitle());
        }

        return String.format(UNASSIGNED_SUCCESS_MESSAGE, workItemId, item.getTitle(), member);
    }
}
