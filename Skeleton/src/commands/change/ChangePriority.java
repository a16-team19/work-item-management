package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Bug;
import models.contracts.Story;
import models.contracts.WorkItem;
import models.workitems.common.PriorityType;
import models.workitems.common.WorkItemType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class ChangePriority extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public ChangePriority(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        PriorityType priority = PriorityType.valueOf(parameters.get(1).toUpperCase().trim());

        return changePriority(workItemId, priority);
    }


    private String changePriority(int workItemId, PriorityType priority) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        if (item.getItemType().equals(WorkItemType.BUG)) {
            Bug bug = (Bug) item;
            bug.setPriority(priority);
        } else if (item.getItemType().equals(WorkItemType.STORY)) {
            Story story = (Story) item;
            story.setPriority(priority);
        } else {
            return String.format(INVALID_ITEM, item.getId(), item.getTitle());
        }

        return String.format(PRIORITY_CHANGED_SUCCESS_MESSAGE, workItemId, item.getTitle(), priority.toString());
    }

}
