package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Feedback;
import models.contracts.WorkItem;
import models.workitems.common.WorkItemType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class ChangeRating extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public ChangeRating(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        int rating = Integer.parseInt(parameters.get(1));

        return changeRating(workItemId, rating);
    }


    private String changeRating(int workItemId, int rating) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        if (item.getItemType().equals(WorkItemType.FEEDBACK)) {
            Feedback feedback = (Feedback) item;
            feedback.setRating(rating);
        } else {
            return String.format(INVALID_ITEM, item.getId(), item.getTitle());
        }

        return String.format(RATING_CHANGED_SUCCESS_MESSAGE, workItemId, item.getTitle(), rating);
    }
}
