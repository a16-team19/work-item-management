package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Story;
import models.contracts.WorkItem;
import models.workitems.common.SizeType;
import models.workitems.common.WorkItemType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class ChangeSize extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public ChangeSize(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        SizeType size = SizeType.valueOf(parameters.get(1).toUpperCase());

        return changeSize(workItemId, size);
    }


    private String changeSize(int workItemId, SizeType size) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        if (item.getItemType().equals(WorkItemType.STORY)) {
            Story story = (Story) item;
            story.setSize(size);
        } else {
            return String.format(INVALID_ITEM, item.getId(), item.getTitle());
        }

        return String.format(SIZE_CHANGED_SUCCESS_MESSAGE, workItemId, item.getTitle(), size.toString());
    }
}
