package commands.change;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.*;
import models.workitems.common.WorkItemType;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class Assign extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String UNASSIGNED = "Unassigned";

    public Assign(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        String member = parameters.get(1);

        return assign(workItemId, member);
    }

    private String assign(int workItemId, String member) {

        WorkItem item = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(item, String.format(INVALID_ITEM_ID, workItemId));

        Member memberToAssign = getMember(member);
        ValidationHelper.checkObjectNull(memberToAssign, String.format(INVALID_MEMBER_NAME, member));

        Board workItemBoard = getWorkItemManagementRepository().getBoards()
                .stream()
                .filter(board -> board.getBoardWorkItems()
                        .contains(item))
                .findAny()
                .get();

        Team workItemTeam = getWorkItemManagementRepository().getTeams()
                .stream()
                .filter(team -> team.getBoards()
                        .contains(workItemBoard))
                .findAny()
                .get();

        if (!workItemTeam.getMembers().contains(memberToAssign)) {
            return String.format(MEMBER_NOT_PART_OF_TEAM, memberToAssign.getName());
        }

        if (item.getItemType().equals(WorkItemType.BUG)) {
            Bug bug = (Bug) item;
            if (!bug.getAssignee().equalsIgnoreCase(UNASSIGNED)) {
                throw new IllegalArgumentException(String.format(ALREADY_ASSIGNED, bug.getAssignee()));
            }
            bug.setAssignee(memberToAssign);
        } else if (item.getItemType().equals(WorkItemType.STORY)) {
            Story story = (Story) item;
            if (!story.getAssignee().equalsIgnoreCase(UNASSIGNED)) {
                throw new IllegalArgumentException(String.format(ALREADY_ASSIGNED, story.getAssignee()));
            }
            story.setAssignee(memberToAssign);
        } else {
            return String.format(INVALID_ITEM, item.getId(), item.getTitle());
        }

        memberToAssign.addWorkItem(item);

        return String.format(ASSIGN_SUCCESS_MESSAGE, workItemId, item.getTitle(), member);
    }

}

