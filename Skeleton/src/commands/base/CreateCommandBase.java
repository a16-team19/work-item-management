package commands.base;

import commands.contracts.CreateCommand;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;

public abstract class CreateCommandBase extends CommandBase implements CreateCommand {

    private final WorkItemManagementFactory workItemManagementFactory;

    protected CreateCommandBase(WorkItemManagementRepository workItemManagementRepository, WorkItemManagementFactory workItemManagementFactory) {
        super(workItemManagementRepository);
        this.workItemManagementFactory = workItemManagementFactory;
    }

    protected WorkItemManagementFactory getWorkItemManagementFactory() {
        return workItemManagementFactory;
    }
}
