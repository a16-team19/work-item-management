package commands.base;

import commands.contracts.Command;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Board;
import models.contracts.Member;
import models.contracts.Team;
import models.contracts.WorkItem;

import java.util.List;

import static commands.constants.CommandConstants.*;

public abstract class CommandBase implements Command {
    private final WorkItemManagementRepository workItemManagementRepository;

    protected CommandBase(WorkItemManagementRepository workItemManagementRepository) {
        this.workItemManagementRepository = workItemManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        ValidationHelper.checkNumberOfArguments(parameters, getCorrectNumberOfArguments());

        return getCommandLogic(parameters);
    }

    protected WorkItemManagementRepository getWorkItemManagementRepository() {
        return workItemManagementRepository;
    }

    protected abstract int getCorrectNumberOfArguments();

    protected abstract String getCommandLogic(List<String> parameters);

    protected String validateCreatingWorkitem(String teamName, String boardName) {
        if (doesTeamExist(teamName)) {
            if (!doesBoardExistTeam(teamName, boardName)) {
                return String.format(INVALID_BOARD_NAME, boardName);
            }
        } else {
            return String.format(INVALID_TEAM_NAME, teamName);
        }
        return VALID;
    }

    protected boolean doesTeamExist(String teamName) {
        return getWorkItemManagementRepository().getTeams()
                .stream()
                .anyMatch(o -> o.getName().equalsIgnoreCase(teamName));
    }

    protected Team getTeam(String teamName) {
        return getWorkItemManagementRepository().getTeams()
                .stream()
                .filter(o -> o.getName().equals(teamName))
                .findAny().orElse(null);
    }

    protected Board getBoardFromTeam(String boardName, String teamName) {
        return getTeam(teamName).getBoards()
                .stream()
                .filter(board -> board.getName().equalsIgnoreCase(boardName))
                .findAny().orElse(null);
    }

    protected boolean doesBoardExistTeam(String teamName, String boardName) {
        Team currentTeam = getTeam(teamName);
        if (currentTeam != null) {
            Board board = getBoardFromTeam(boardName, teamName);
            return board != null;
        } else {
            return false;
        }
    }

    protected boolean doesMemberExist(String memberName) {
        return getWorkItemManagementRepository().getMembers().containsKey(memberName);
    }

    protected Member getMember(String memberName) {
        return getWorkItemManagementRepository().getMembers().get(memberName);
    }

    protected WorkItem getWorkItem(int workItemId) {
        return workItemManagementRepository.getWorkItems().get(workItemId);
    }
}

