package commands.add;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Member;
import models.contracts.WorkItem;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class AddComment extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;

    public AddComment(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        int workItemId = Integer.parseInt(parameters.get(0));
        String member = parameters.get(1);
        String comment = parameters.get(2);

        return addComment(workItemId, member, comment);
    }

    private String addComment(int workItemId, String member, String comment) {

        WorkItem workItem = getWorkItem(workItemId);
        ValidationHelper.checkObjectNull(workItem, String.format(INVALID_ITEM_ID, workItemId));

        Member actualMember = getMember(member);
        ValidationHelper.checkObjectNull(actualMember, String.format(INVALID_MEMBER_NAME,member));

        workItem.addComment(member, comment);

        return String.format(COMMENT_SUCCESS_MESSAGE, comment, workItemId, workItem.getTitle(), member);
    }
}
