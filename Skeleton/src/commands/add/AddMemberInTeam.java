package commands.add;

import commands.base.CommandBase;
import core.contracts.WorkItemManagementRepository;
import models.ValidationHelper;
import models.contracts.Member;
import models.contracts.Team;

import java.util.List;

import static commands.constants.CommandConstants.*;

public class AddMemberInTeam extends CommandBase {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    public AddMemberInTeam(WorkItemManagementRepository workItemManagementRepository) {
        super(workItemManagementRepository);
    }

    @Override
    public int getCorrectNumberOfArguments() {
        return CORRECT_NUMBER_OF_ARGUMENTS;
    }

    @Override
    public String getCommandLogic(List<String> parameters) {

        String teamName = parameters.get(0);
        String memberName = parameters.get(1);

        return addMemberToTeam(teamName, memberName);
    }

    private String addMemberToTeam(String teamName, String memberName) {

        Team team = getTeam(teamName);
        ValidationHelper.checkObjectNull(team, String.format(INVALID_TEAM_NAME, teamName));

        Member member = getMember(memberName);
        ValidationHelper.checkObjectNull(member, String.format(INVALID_MEMBER_NAME, memberName));

        team.addMember(member);

        return String.format(MEMBER_ADDED_TEAM_SUCCESS_MESSAGE, member.getName(), team.getName());
    }

}


