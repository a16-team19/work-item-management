package wim.models;

import models.contracts.Feedback;
import models.workitems.FeedbackImpl;
import org.junit.Test;

public class FeedbackImpl_Tests {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_title_length() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("sad", "asdasdsadsadsadasddsa", 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_description_length() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("asdasdsadasddsadasdasdasd", "sad", 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when__ratingLessThanMin() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("asdasdsadasdasddsda", "sadasdsadsadsdsadas", -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when__ratingMoreThanMax() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("asdasdsadasdasddsda", "sadasdsadsadsdsadas", 11);
    }
}
