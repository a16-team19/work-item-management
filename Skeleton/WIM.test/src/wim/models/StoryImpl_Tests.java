package wim.models;

import models.contracts.Story;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;
import org.junit.Test;

public class StoryImpl_Tests {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_title_length() {
        // Arrange, Act, Assert
        Story story = new StoryImpl("asd", "asdasdasdasdasdsad", PriorityType.LOW, SizeType.LARGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_description_length() {
        // Arrange, Act, Assert
        Story story = new StoryImpl("asdsadsadsadsadsadsadas", "as", PriorityType.LOW, SizeType.LARGE);
    }
}
