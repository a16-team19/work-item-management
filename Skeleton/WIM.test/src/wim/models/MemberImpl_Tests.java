package wim.models;

import models.MemberImpl;
import models.contracts.Member;
import org.junit.Test;

public class MemberImpl_Tests {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_name_length() {
        // Arrange, Act, Assert
        Member member = new MemberImpl("asds");
    }
}