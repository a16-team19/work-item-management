package wim.models;

import models.contracts.Bug;
import models.workitems.BugImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BugImpl_Tests {

    private List<String> steps;

    @Before
    public void before() {
        steps = new ArrayList<>();
        steps.add("sad");
        steps.add("sdsd");
        steps.add("sd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_title_length() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("asd", "fjksabdjskanbasd", steps, PriorityType.LOW, SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_description_length() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("asddsadsadsadsadas", "fjk", steps, PriorityType.LOW, SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_stepsIsNull() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("asddsadsadsadsadas", "fjk", null, PriorityType.LOW, SeverityType.MAJOR);
    }
}
