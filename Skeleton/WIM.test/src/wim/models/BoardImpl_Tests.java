package wim.models;

import models.BoardImpl;
import models.contracts.Board;
import org.junit.Test;

public class BoardImpl_Tests {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_invalid_name_length() {

        // Arrange, Act, Assert
        Board board = new BoardImpl("asd");
    }
}
