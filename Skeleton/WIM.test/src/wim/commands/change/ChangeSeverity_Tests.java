package wim.commands.change;

import commands.change.ChangeSeverity;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Bug;
import models.workitems.BugImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeSeverity_Tests {

    private Command testCommand;
    private Bug testBug;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ChangeSeverity(workItemManagementRepository);
        List<String> testSteps = new ArrayList<>();
        testSteps.add("first");
        testSteps.add("second");
        testBug = new BugImpl("BugImplTitle", "BugImplDescription", testSteps, PriorityType.LOW, SeverityType.MINOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testBug.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testBug.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("CRITICAL");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("CRITICAL");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(SeverityType.CRITICAL, testBug.getSeverity());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_severityDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testBug.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("nqma");

        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_changeSeverity_whenInputIsCorrect() {
        String testId = String.valueOf(testBug.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("CRITICAL");

        workItemManagementRepository.addWorkItem(testBug.getId(), testBug);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(SeverityType.CRITICAL, testBug.getSeverity());
    }
}
