package wim.commands.change;

import commands.change.ChangeRating;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Feedback;
import models.workitems.FeedbackImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeRating_Tests {

    private Command testCommand;
    private Feedback testFeedback;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ChangeRating(workItemManagementRepository);
        testFeedback = new FeedbackImpl("FeedbackImplTitle","FeedbackImplDescription",1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testFeedback.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testFeedback.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("5");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("5");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(5,testFeedback.getRating());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_ratingIsLessThanMaxValueg() {
        // Arrange
        String testId = String.valueOf(testFeedback.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("-1");

        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_ratingIsMoreThanMaxValue() {
        // Arrange
        String testId = String.valueOf(testFeedback.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("11");

        // Act, Assert
        testCommand.execute(testList);
    }
    @Test
    public void getCommandLogic_should_changeRating_whenInputIsCorrect() {
        String testId = String.valueOf(testFeedback.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("5");

        workItemManagementRepository.addWorkItem(testFeedback.getId(), testFeedback);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(5,testFeedback.getRating());
    }
}
