package wim.commands.change;

import commands.change.ChangeStatus;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Story;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;
import models.workitems.common.StoryStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStatus_Tests {

    private Command testCommand;
    private Story testStory;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ChangeStatus(workItemManagementRepository);
        testStory = new StoryImpl("FeedbackImplTitle", "FeedbackImplDescription", PriorityType.LOW, SizeType.LARGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("DONE");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("DONE");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(StoryStatusType.DONE, testStory.getStatus());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_storyStatusDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("nqma");

        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_changeStoryStatus_whenInputIsCorrect() {
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("DONE");

        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(StoryStatusType.DONE, testStory.getStatus());
    }
}
