package wim.commands.change;

import commands.change.Assign;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.BoardImpl;
import models.MemberImpl;
import models.TeamImpl;
import models.contracts.*;
import models.workitems.BugImpl;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import models.workitems.common.SizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Assign_Tests {

    private Command testCommand;
    private Team testTeam;
    private Board testBoard;
    private Story testStory;
    private Bug testBug;
    private Member testMember;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new Assign(workItemManagementRepository);
        testTeam = new TeamImpl("TestTeam");
        testBoard = new BoardImpl("TestBoard");
        testStory = new StoryImpl("FeedbackImplTitle", "FeedbackImplDescription",
                PriorityType.LOW, SizeType.LARGE);
        List<String> bugSteps = new ArrayList<>();
        bugSteps.add("first");
        bugSteps.add("second");
        testBug = new BugImpl("BugImplTest","BugImplTest", bugSteps,
                PriorityType.LOW, SeverityType.MINOR);
        testMember = new MemberImpl("TestMember");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("TestMember");

        workItemManagementRepository.addMember(testMember.getName(), testMember);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testStory.getComments().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");

        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);

        // Act
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberAlreadyAssigned() {
        // Arrange
        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);
        workItemManagementRepository.addMember(testMember.getName(), testMember);
        workItemManagementRepository.addTeam(testTeam);
        workItemManagementRepository.addBoard(testBoard);

        testBoard.addWorkItem(testStory);
        testTeam.addMember(testMember);
        testTeam.addBoard(testBoard);
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");

        // Act, Assert
        testCommand.execute(testList);
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_assignStory_whenInputIsCorrect() {
        // Arrange
        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);
        workItemManagementRepository.addMember(testMember.getName(), testMember);
        workItemManagementRepository.addTeam(testTeam);
        workItemManagementRepository.addBoard(testBoard);

        testBoard.addWorkItem(testStory);
        testTeam.addMember(testMember);
        testTeam.addBoard(testBoard);
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add(testMember.getName());
        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(testMember.getName(), testStory.getAssignee());
        Assert.assertEquals(1,testMember.getMemberWorkItems().size());
    }

    @Test
    public void getCommandLogic_should_assignBug_whenInputIsCorrect() {
        // Arrange
        workItemManagementRepository.addTeam(testTeam);
        workItemManagementRepository.addWorkItem(testBug.getId(), testBug);
        workItemManagementRepository.addBoard(testBoard);
        workItemManagementRepository.addMember("TestMember", testMember);

        testBoard.addWorkItem(testBug);
        testTeam.addMember(testMember);
        testTeam.addBoard(testBoard);
        String testId = String.valueOf(testBug.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add(testMember.getName());

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(testMember.getName(), testBug.getAssignee());
        Assert.assertEquals(1,testMember.getMemberWorkItems().size());
    }
}
