package wim.commands.change;

import commands.change.UnAssign;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.contracts.Member;
import models.contracts.Story;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UnAssign_Tests {

    private Command testCommand;
    private Story testStory;
    private Member testMember;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new UnAssign(workItemManagementRepository);
        testStory = new StoryImpl("FeedbackImplTitle", "FeedbackImplDescription", PriorityType.LOW, SizeType.LARGE);
        testMember = new MemberImpl("TestMember");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("TestMember1");

        workItemManagementRepository.addMember("TestMember1", testMember);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testStory.getComments().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");

        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);

        // Act
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_assign_whenInputIsCorrect() {
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");

        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);
        workItemManagementRepository.addMember("TestMember1", testMember);

        // Act
        testStory.setAssignee(testMember);
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals("Unassigned", testStory.getAssignee());
        Assert.assertEquals(0,testMember.getMemberWorkItems().size());
    }
}
