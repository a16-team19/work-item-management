package wim.commands.change;

import commands.change.ChangeSize;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Story;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeSize_Tests {

    private Command testCommand;
    private Story testStory;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ChangeSize(workItemManagementRepository);
        testStory = new StoryImpl("StoryTitle", "StoryDescription", PriorityType.LOW, SizeType.SMALL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("LARGE");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("LARGE");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(SizeType.LARGE, testStory.getSize());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sizeDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("nqma");

        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_changeSize_whenInputIsCorrect() {
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("LARGE");

        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(SizeType.LARGE, testStory.getSize());
    }
}
