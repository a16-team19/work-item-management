package wim.commands.change;

import commands.change.ChangePriority;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.Bug;
import models.contracts.Story;
import models.workitems.BugImpl;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import models.workitems.common.SizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangePriority_Tests {

    private Command testCommand;
    private Story testStory;
    private Bug testBug;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ChangePriority(workItemManagementRepository);
        testStory = new StoryImpl("FeedbackImplTitle", "FeedbackImplDescription",
                PriorityType.LOW, SizeType.LARGE);
        List<String> bugSteps = new ArrayList<>();
        bugSteps.add("first");
        bugSteps.add("second");
        testBug = new BugImpl("BugImplTest","BugImplTest", bugSteps,
                PriorityType.LOW, SeverityType.MINOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("HIGH");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("HIGH");

        // Act
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_priorityDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("nqma");

        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_changeStoryPriority_whenInputIsCorrect() {
        String testId = String.valueOf(testStory.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("HIGH");

        workItemManagementRepository.addWorkItem(testStory.getId(), testStory);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(PriorityType.HIGH, testStory.getPriority());
    }

    @Test
    public void getCommandLogic_should_changeBugPriority_whenInputIsCorrect() {
        String testId = String.valueOf(testBug.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("MEDIUM");

        workItemManagementRepository.addWorkItem(testBug.getId(), testBug);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(PriorityType.MEDIUM, testBug.getPriority());
    }
}
