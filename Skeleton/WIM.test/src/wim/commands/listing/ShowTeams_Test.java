package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ShowTeams;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.TeamImpl;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.NEW_COMMAND_DELIMITER;
import static commands.constants.CommandConstants.TEAMS_EMPTY;

public class ShowTeams_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ShowTeams(workItemManagementRepository);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ShowTeams");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenNoTeamsExist() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(TEAMS_EMPTY,executeMsg);
    }

    @Test
    public void getCommandLogic_should_print_whenOneTeamExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        Team team = new TeamImpl("qgodki");
        workItemManagementRepository.addTeam(team);
        String expected = NEW_COMMAND_DELIMITER + System.lineSeparator() + team.getName();


        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected,executeMsg);
    }
}
