package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.FilterStastusAndAssignMember;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FilterStatusAndAssignMember_Tests {

    private Command testCommand;
    private Member testMember;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new FilterStastusAndAssignMember(workItemManagementRepository);
        testMember = new MemberImpl("TestMember");
        workItemManagementRepository.addMember("TestMember", testMember);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("DONE");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("DONE");
        testList.add("TestMember");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenNoItemsWithStatusFixed() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FIXED");
        testList.add("TestMember");

        // Assert
        Assert.assertEquals("There is not a single work item here with status FIXED and assigned to TestMember!", testCommand.execute(testList));
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("DONE");
        testList.add("WrongMember");
        workItemManagementRepository.addMember(testMember.getName(), testMember);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenMemberDoesNotHaveAssigns() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("DONE");
        testList.add("TestMember");

        // Act, Assert
        Assert.assertEquals("There is not a single work item here with status DONE and assigned to TestMember!", testCommand.execute(testList));

    }
}
