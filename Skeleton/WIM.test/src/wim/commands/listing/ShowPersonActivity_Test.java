package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ShowPersonActivity;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;

public class ShowPersonActivity_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;
    private Member testMember;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ShowPersonActivity(workItemManagementRepository);
        testMember = new MemberImpl("pesho");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ShowPersonActivity");
        testList.add("pesho");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("WrongMember");
        workItemManagementRepository.addMember(testMember.getName(), testMember);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenMemberActivityEmpty() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testMember.getName());
        String expected = String.format(PERSON_ACTIVITIES_EMPTY,testMember.getName());

        workItemManagementRepository.addMember(testMember.getName(),testMember);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected,executeMsg);
    }
}
