package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ShowBoardActivity;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.BoardImpl;
import models.TeamImpl;
import models.contracts.Board;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.BOARD_ACTIVITIES_EMPTY;

public class ShowBoardActivity_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;
    private Team testTeam;
    private Board testBoard;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ShowBoardActivity(workItemManagementRepository);
        testTeam = new TeamImpl("qgodki");
        testBoard = new BoardImpl("newBoard");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ShowBoardActivity");
        testList.add("qgodki");
        testList.add("newboard");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add("WrongBoard");
        workItemManagementRepository.addBoard(testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenBoardActivityEmpty() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add(testBoard.getName());

        testTeam.addBoard(testBoard);
        workItemManagementRepository.addTeam(testTeam);
        workItemManagementRepository.addBoard(testBoard);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(BOARD_ACTIVITIES_EMPTY,executeMsg);
    }
}

