package wim.commands.listing;

import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.TeamImpl;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.TEAM_ACTIVITIES_EMPTY;

public class ShowTeamActivity_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;
    private Team testTeam;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new commands.listing.ShowTeamActivity(workItemManagementRepository);
        testTeam = new TeamImpl("qgodki");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ShowTeamActivity");
        testList.add("pesho");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("WrongTeam");
        workItemManagementRepository.addTeam(testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenTeamActivityEmpty() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());

        workItemManagementRepository.addTeam(testTeam);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(TEAM_ACTIVITIES_EMPTY,executeMsg);
    }
}

