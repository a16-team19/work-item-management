package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ListComments;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.contracts.Member;
import models.contracts.WorkItem;
import models.workitems.BugImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;

public class ListComments_Test {
    private WorkItemManagementRepository workItemManagementRepository;
    private Command testCommand;
    private WorkItem testWorkItem;

    @Before
    public void before(){
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ListComments(workItemManagementRepository);
        testWorkItem = new BugImpl("newbug222222", "bigbug123321", new ArrayList<>(), PriorityType.MEDIUM, SeverityType.MAJOR);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ListComments");
        testList.add("0");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_print_when_NoComments(){
        // Arrange
        List<String> testList = new ArrayList<>();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(),testWorkItem);
        testList.add(String.valueOf(testWorkItem.getId()));

        // Act
        String executeMsg = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(NO_COMMENTS,executeMsg);
    }

    @Test
    public void execute_should_print_when_OneComment(){
        // Arrange
        List<String> testList = new ArrayList<>();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(),testWorkItem);
        testList.add(String.valueOf(testWorkItem.getId()));
        Member member = new MemberImpl("pesho");

        testWorkItem.addComment(member.getName(),"Ala bala portokala!");
        String expected = (NEW_COMMAND_DELIMITER + System.lineSeparator() + member.getName() + COLON +
                testWorkItem.getComments().get(member.getName())).replace("[","").replace("]","");

        // Act & Assert
        String executeMsg = testCommand.execute(testList);

        Assert.assertEquals(expected,executeMsg);
    }
}

