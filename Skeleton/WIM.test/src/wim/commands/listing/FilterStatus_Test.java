package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.FilterStatus;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.WorkItem;
import models.workitems.StoryImpl;
import models.workitems.common.BugStatusType;
import models.workitems.common.PriorityType;
import models.workitems.common.SizeType;
import models.workitems.common.StoryStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;


public class FilterStatus_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;


    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new FilterStatus(workItemManagementRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FilterStatus");
        testList.add("test argument");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenNoItemsWithStatusFixed() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(BugStatusType.FIXED.toString());
        String expected = String.format(WORKITEM_STATUS_EMPTY,BugStatusType.FIXED.toString());

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected,executeMsg);
    }

    @Test
    public void getCommandLogic_should_print_whenOneItemWithStatusNotDone() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(StoryStatusType.NOTDONE.toString());
        WorkItem testWorkItem = new StoryImpl("newstory123321", "bigstory123321", PriorityType.HIGH, SizeType.LARGE);
        testWorkItem.setStatus(StoryStatusType.NOTDONE);
        workItemManagementRepository.addWorkItem(testWorkItem.getId(),testWorkItem);
        String expected = NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected,executeMsg);
    }
}
