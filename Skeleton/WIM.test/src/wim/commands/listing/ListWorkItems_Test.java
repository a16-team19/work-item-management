package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ListWorkItems;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.WorkItem;
import models.workitems.BugImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;

public class ListWorkItems_Test {
    private WorkItemManagementRepository workItemManagementRepository;
    private Command testCommand;
    private WorkItem testWorkItem;

    @Before
    public void before(){
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ListWorkItems(workItemManagementRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ListWorkitems");
        testList.add("ALL");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_print_when_NoWorkItems(){
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");

        // Act & Assert
        String executeMsg = testCommand.execute(testList);

        Assert.assertEquals(WORK_ITEM_EMPTY,executeMsg);
    }

    @Test
    public void execute_should_print_when_OneWorkItemPrintAll(){
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");
        testWorkItem = new BugImpl("newbug222222", "bigbug123321", new ArrayList<>(), PriorityType.MEDIUM, SeverityType.MAJOR);

        String expected = NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();

        workItemManagementRepository.addWorkItem(testWorkItem.getId(),testWorkItem);
        // Act & Assert
        String executeMsg = testCommand.execute(testList);

        Assert.assertEquals(expected,executeMsg);
    }

    @Test
    public void execute_should_print_when_OneWorkItemPrintBug(){
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("BUG");
        testWorkItem = new BugImpl("newbug222222", "bigbug123321", new ArrayList<>(), PriorityType.MEDIUM, SeverityType.MAJOR);
        String expected = NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(),testWorkItem);

        // Act & Assert
        String executeMsg = testCommand.execute(testList);

        Assert.assertEquals(expected,executeMsg);
    }
}
