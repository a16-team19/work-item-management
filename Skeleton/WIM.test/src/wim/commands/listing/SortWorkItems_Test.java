package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.SortWorkItems;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.contracts.WorkItem;
import models.workitems.BugImpl;
import models.workitems.FeedbackImpl;
import models.workitems.StoryImpl;
import models.workitems.common.PriorityType;
import models.workitems.common.SeverityType;
import models.workitems.common.SizeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.NEW_COMMAND_DELIMITER;
import static commands.constants.CommandConstants.WORKITEM_PRINT;

public class SortWorkItems_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;
    private WorkItem testWorkItem;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new SortWorkItems(workItemManagementRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("SortWorkItems");
        testList.add("Bug");
        testList.add("Story");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_all_priority() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");
        testList.add("PRIORITY");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_all_severity() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");
        testList.add("SEVERITY");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_all_size() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");
        testList.add("SIZE");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_all_rating() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");
        testList.add("RATING");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_bug_rating() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("BUG");
        testList.add("RATING");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_bug_size() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("BUG");
        testList.add("SIZE");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_story_rating() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("STORY");
        testList.add("RATING");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_story_severity() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("STORY");
        testList.add("SEVERITY");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_feedback_severity() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FEEDBACK");
        testList.add("SEVERITY");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_feedback_size() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FEEDBACK");
        testList.add("SIZE");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sort_feedback_priority() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FEEDBACK");
        testList.add("PRIORITY");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenBugSortedTitle() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("BUG");
        testList.add("TITLE");
        testWorkItem = new BugImpl("newbug222222", "bigbug123321", new ArrayList<>(), PriorityType.MEDIUM, SeverityType.MAJOR);
        String expected = WORKITEM_PRINT + System.lineSeparator() +NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(), testWorkItem);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected, executeMsg);
    }

    @Test
    public void getCommandLogic_should_print_whenAllSortedTitle() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ALL");
        testList.add("TITLE");
        testWorkItem = new BugImpl("newbug222222", "bigbug123321", new ArrayList<>(), PriorityType.MEDIUM, SeverityType.MAJOR);
        String expected = WORKITEM_PRINT + System.lineSeparator() +NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(), testWorkItem);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected, executeMsg);
    }

    @Test
    public void getCommandLogic_should_print_whenStorySortedPriority() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("STORY");
        testList.add("PRIORITY");
        testWorkItem = new StoryImpl("newstory123321", "bigstory123321", PriorityType.MEDIUM, SizeType.LARGE);
        String expected =WORKITEM_PRINT + System.lineSeparator() + NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(), testWorkItem);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected, executeMsg);
    }

    @Test
    public void getCommandLogic_should_print_whenFeedbackSortedRating() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FEEDBACK");
        testList.add("RATING");
        testWorkItem = new FeedbackImpl("newfeedback123321","bigfeedback123", 5);
        String expected =WORKITEM_PRINT + System.lineSeparator() + NEW_COMMAND_DELIMITER + System.lineSeparator() + testWorkItem.toString();
        workItemManagementRepository.addWorkItem(testWorkItem.getId(), testWorkItem);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected, executeMsg);
    }
}


