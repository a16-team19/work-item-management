package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ShowTeamMembers;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.TeamImpl;
import models.contracts.Member;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;

public class ShowTeamMembers_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;
    private Team testTeam;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ShowTeamMembers(workItemManagementRepository);
        testTeam = new TeamImpl("qgodki");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ShowTeamMembers");
        testList.add("pesho");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("WrongTeam");
        workItemManagementRepository.addTeam(testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenTeamHasNoMembers() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        String expected = String.format(ZERO_MEMBERS_TEAM,testTeam.getName());
        workItemManagementRepository.addTeam(testTeam);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected, executeMsg);

    }

    @Test
    public void getCommandLogic_should_print_whenTeamHasOneMember() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        Member member = new MemberImpl("pesho");
        testTeam.addMember(member);
        String expected = String.format(TEAM, testTeam.getName()) + JOIN_DELIMITER + System.lineSeparator()
                + member.toString();
        workItemManagementRepository.addTeam(testTeam);

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected, executeMsg);

    }
}
