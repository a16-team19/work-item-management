package wim.commands.listing;

import commands.contracts.Command;
import commands.listing.ShowMembers;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static commands.constants.CommandConstants.*;

public class ShowMembers_Test {
    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new ShowMembers(workItemManagementRepository);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("ShowMembers");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_print_whenNoMembersExist() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(MEMBERS_EMPTY,executeMsg);
    }

    @Test
    public void getCommandLogic_should_print_whenOneMembersExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        Member member = new MemberImpl("pesho");
        workItemManagementRepository.addMember(member.getName(),member);
        String expected = MEMBERS_PRINT + System.lineSeparator() + member.getName();


        // Act
        String executeMsg = testCommand.execute(testList);

        // Assert
        Assert.assertEquals(expected,executeMsg);
    }
}
