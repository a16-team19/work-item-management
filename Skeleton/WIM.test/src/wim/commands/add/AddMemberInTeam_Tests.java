package wim.commands.add;

import commands.add.AddMemberInTeam;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.TeamImpl;
import models.contracts.Member;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddMemberInTeam_Tests {

    private Command testCommand;
    private Team testTeam;
    private Member testMember;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new AddMemberInTeam(workItemManagementRepository);
        testTeam = new TeamImpl("TestTeam");
        testMember = new MemberImpl("TestMember1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCommandLogic_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("TestMember1");
        testList.add("moreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("WrongTeam");
        testList.add("TestMember1");
        workItemManagementRepository.addMember("TestMember1", testMember);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("WrongMember");
        workItemManagementRepository.addTeam(testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_addMemberInTeam_whenInputIsCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("TestMember1");

        workItemManagementRepository.addTeam(testTeam);
        workItemManagementRepository.addMember("TestMember1", testMember);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testTeam.getMembers().size());
    }

}
