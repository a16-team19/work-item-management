package wim.commands.add;

import commands.add.AddComment;
import commands.contracts.Command;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementRepository;
import models.MemberImpl;
import models.contracts.Member;
import models.contracts.WorkItem;
import models.workitems.FeedbackImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddComment_Tests {

    private Command testCommand;
    private WorkItem testWorkItem;
    private Member testMember;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        testCommand = new AddComment(workItemManagementRepository);
        testWorkItem = new FeedbackImpl("FeedbackImplTitle", "FeedbackImplDescription", 5);
        testMember = new MemberImpl("TestMember");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        String testId = String.valueOf(testWorkItem.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        String testId = String.valueOf(testWorkItem.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");
        testList.add("Comment");
        testList.add("MoreArguments");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-1");
        testList.add("TestMember1");
        testList.add("Comment");

        workItemManagementRepository.addMember("TestMember1", testMember);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testWorkItem.getComments().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        // Arrange
        String testId = String.valueOf(testWorkItem.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");
        testList.add("Comment");

        workItemManagementRepository.addWorkItem(testWorkItem.getId(), testWorkItem);

        // Act
        testCommand.execute(testList);
    }

    @Test
    public void getCommandLogic_should_addComment_whenInputIsCorrect() {
        // Arrange
        String testId = String.valueOf(testWorkItem.getId());
        List<String> testList = new ArrayList<>();
        testList.add(testId);
        testList.add("TestMember1");
        testList.add("Comment");

        workItemManagementRepository.addWorkItem(testWorkItem.getId(), testWorkItem);
        workItemManagementRepository.addMember("TestMember1", testMember);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testWorkItem.getComments().size());
    }
}
