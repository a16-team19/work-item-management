package wim.commands.creating;

import commands.contracts.Command;
import commands.creating.CreateBoard;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import core.factories.WorkItemManagementFactoryImpl;
import models.TeamImpl;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBoard_Tests {

    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        WorkItemManagementFactory workItemManagementFactory = new WorkItemManagementFactoryImpl();
        testCommand = new CreateBoard(workItemManagementRepository, workItemManagementFactory);
        Team testTeam = new TeamImpl("TestTeam");
        workItemManagementRepository.addTeam(testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("Board");
        testList.add("MoreArguments");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("WRONG");
        testList.add("Board");
        // Act, Assert
        Assert.assertEquals("Team WRONG does not exist!",testCommand.execute(testList));
    }

    @Test
    public void execute_should_throwException_when_boardAlreadyExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("Board");

        // Act
        testCommand.execute(testList);
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals("Board Board already exist in TestTeam team!",testCommand.execute(testList));
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_titleIsLessThenMinValue() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("ABCD");
        testList.add("MoreArguments");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_titleIsMoreThenMaxValue() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("BoardBoard");
        testList.add("MoreArguments");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBoard_whenPassedValidArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("Board");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, workItemManagementRepository.getBoards().size());
    }
}
