package wim.commands.creating;

import commands.contracts.Command;
import commands.creating.CreateStory;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import core.factories.WorkItemManagementFactoryImpl;
import models.BoardImpl;
import models.TeamImpl;
import models.contracts.Board;
import models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateStory_Tests {

    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        WorkItemManagementFactory workItemManagementFactory = new WorkItemManagementFactoryImpl();
        testCommand = new CreateStory(workItemManagementRepository, workItemManagementFactory);
        Team testTeam = new TeamImpl("TestTeam");
        Board testBoard = new BoardImpl("TestBoard");
        testTeam.addBoard(testBoard);
        workItemManagementRepository.addTeam(testTeam);
        workItemManagementRepository.addBoard(testBoard);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("TestBoard");
        testList.add("StoryTitle");
        testList.add("StoryDescription");
        testList.add("LOW");

        // Act ,Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("TestBoard");
        testList.add("StoryTitle");
        testList.add("StoryDescription");
        testList.add("LOW");
        testList.add("SMALL");
        testList.add("MoreArguments");

        // Act ,Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("WrongTeam");
        testList.add("TestBoard");
        testList.add("StoryTitle");
        testList.add("StoryDescription");
        testList.add("LOW");
        testList.add("SMALL");

        // Act, Assert
        Assert.assertEquals("Team WrongTeam does not exist!", testCommand.execute(testList));
    }

    @Test
    public void execute_should_throwException_when_boardDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("WrongBoard");
        testList.add("StoryTitle");
        testList.add("StoryDescription");
        testList.add("LOW");
        testList.add("SMALL");

        // Act, Assert
        Assert.assertEquals("Board WrongBoard does not exist!", testCommand.execute(testList));
    }

    @Test
    public void execute_should_createStory_whenPassedValidArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("TestBoard");
        testList.add("StoryTitle");
        testList.add("StoryDescription");
        testList.add("LOW");
        testList.add("SMALL");


        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, workItemManagementRepository.getWorkItems().size());
    }
}
