package wim.commands.creating;

import commands.contracts.Command;
import commands.creating.CreateTeam;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateTeam_Tests {

    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        WorkItemManagementFactory workItemManagementFactory = new WorkItemManagementFactoryImpl();
        testCommand = new CreateTeam(workItemManagementRepository, workItemManagementFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act ,Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");
        testList.add("MoreArguments");

        // Act ,Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_throwException_when_teamAlreadyExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");

        // Act
        testCommand.execute(testList);
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Team TestTeam already exists!", testCommand.execute(testList));
    }

    @Test
    public void execute_should_createMember_whenPassedValidArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestTeam");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, workItemManagementRepository.getTeams().size());
    }
}
