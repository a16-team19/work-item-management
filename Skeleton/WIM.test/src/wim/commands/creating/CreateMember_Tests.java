package wim.commands.creating;

import commands.contracts.Command;
import commands.creating.CreateMember;
import core.WorkItemManagementRepositoryImpl;
import core.contracts.WorkItemManagementFactory;
import core.contracts.WorkItemManagementRepository;
import core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateMember_Tests {

    private Command testCommand;
    private WorkItemManagementRepository workItemManagementRepository;

    @Before
    public void before() {
        workItemManagementRepository = new WorkItemManagementRepositoryImpl();
        WorkItemManagementFactory workItemManagementFactory = new WorkItemManagementFactoryImpl();
        testCommand = new CreateMember(workItemManagementRepository, workItemManagementFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act ,Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestMember");
        testList.add("MoreArguments");

        // Act ,Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_throwException_when_memberAlreadyExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestMember");

        // Act
        testCommand.execute(testList);
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Member TestMember already exist!", testCommand.execute(testList));
    }

    @Test
    public void execute_should_createMember_whenPassedValidArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("TestMember");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, workItemManagementRepository.getMembers().size());
    }
}
