## OOP Teamwork Assignment - **Work Item Management (WIM)**

[For Trello click here](https://trello.com/b/7sqeDzfC/teamwork1)

Work Item Management (WIM) Console Application should support multiple teams. Each team has name, members and boards. 

-Each team has **name, members and boards**. 
-Each member has **name, list of work items and activity history**.
-Each board has **name, list of work items and activity history**.

- Work items are three types: **bug, story and feedback**. 

  - Each work item has **id, title, description, comments and activity history**. 
   - Each bug has additional **steps, priority, severity and assignee**. Each bug can adjust priority, severity, status and assign Member to it.
   - Each feedback has additional **rating**. Each feedback can adjust status and rating.
   - Each story has additional **priority, size and assignee**. Each story can adjust priority, size, status and assign Member to it.
 
- Each board can add or remove work items to them.

### Validation

- Work item:
  - Title can not be **less than 10 or more than 50 symbols**.
  - Description can not be **less than 10 and more than 500symbols**.
- Bug:
  -Priority is one of the following: **High, Medium, Low**
  -Severity is one of the following: **Critical, Major, Minor**
  -Status is one of the following: **Active, Fixed**
  -Assignee is a member from the team.
- Story
  -Priority is one of the following: **High, Medium, Low**
  -Size is one of the following: **Large, Medium, Small**
  -Status is one of the following: **NotDone, InProgress, Done**
  -Assignee is a member from the team.
- Feedback
  -Rating is a whole number between **0 - 10**.
  - Status is one of the following: **New, Unscheduled, Scheduled, Done**
- Adjustable chair:
    - Can change the height to a new valid one. 

- Board
  -Name should be **unique** in the team
  -Name is a string between **5 and 10 symbols**.
- Member
  -Name should be **unique** in the application
  -Name is a string between **5 and 15 symbols**.

### Commands
  **Create Commands**:

	CreateMember (MemberName) 

	CreateTeam (TeamName)

	CreateBoard (TeamName)_(BoardName)

	CreateBug (TeamName)_(BoardName)_(BugTitle)_(Steps with comma)_(Priority)_(Severity)

	CreateStory (TeamName)_(BoardName)_(StoryTitle)_(Priority)_(Size)

	CreateFeedback (TeamName)_(BoardName)_(FeedbackTitle)_(Rating)

  **Print commands**:

	ListComments (WorkItemID)

	SortWorkItems (All,WorkItemType)_(SortType)

	FilterAssign (MemberName)

	FilterStatus (Status)

	FilterStatusAssignee (Status)_(MemberName)

	ListWorkItems (All,WorkItemType)

	ShowBoardActivity (TeamName_(BoardName)

	ShowPersonActivity (MemberName)

	ShowTeamActivity (TeamName)

	ShowTeamBoards (TeamName)

	ShowTeamMembers (TeamName)

	ShowTeams

	ShowMembers

  **Add Commands**:

	AddComment (WorkItemID)_(MemberName)_(Comment)

	AddMemberInTeam (TeamName)_(MemberName)

  **Change Commands**:
  
	Assign (WorkItemID)_(MemberName)

	ChangePriority (WorkItemID)_(Priority)

	ChangeRating (WorkItemID)_(Rating)

	ChangeSeverity (WorkItemID)_(Severity)

	ChangeSize (WorkItemID)_(Size)

	ChangeStatus (WorkItemID)_(Status)

	UnAssign (WorkItemID)_(MemberName)

### Sample Input

```
createmember pesho
createmember sasho
createteam qgodki
addmember qgodki_pesho
addmember qgodki_sasho
createboard qgodki_board123
createbug qgodki_board123_newbug222222_bigbug123321_step1,step2_HIGH_major
createstory qgodki_board123_newstory123321_bigstory123321_medium_small
createfeedback qgodki_board123_newfeedback123321_bigfeedback123_5
addcomment 0_pesho_ala bala portokala
addcomment 0_pesho_ala bala oranjada
addcomment 0_sasho_khvbkhj buiyigv ,kjbj
addcomment 0_sasho_khvbkhjyuvfuyv ,khvljh
listcomments 0
sortworkitems all_title
assign 0_sasho
assign 0_pesho
filterassign pesho
changepriority 0_high
changerating 2_10
changeseverity 0_major
changesize 1_Large
changestatus 1_done
sortworkitems bug_severity
sortworkitems bug_priority
sortworkitems story_priority
sortworkitems story_size
sortworkitems feedback_title
sortworkitems feedback_rating
showpersonactivity pesho
showteamboards qgodki
showmembers
showboardactivity qgodki_board123
showpersonactivity pesho
showteamactivity qgodki
showteammembers qgodki
Exit
```

### Sample Output
```
Member pesho created!
Member sasho created!
Team qgodki created!
Member pesho added in team qgodki!
Member sasho added in team qgodki!
Board board123 created in team qgodki!
Added work item :Bug #0 newbug222222 to qgodki board123!
Added work item :Story #1 newstory123321 to qgodki board123!
Added work item :Feedback #2 newfeedback123321 to qgodki board123!
Added comment :ala bala portokala to #0 newbug222222 from pesho
Added comment :ala bala oranjada to #0 newbug222222 from pesho
Added comment :khvbkhj buiyigv ,kjbj to #0 newbug222222 from sasho
Added comment :khvbkhjyuvfuyv ,khvljh to #0 newbug222222 from sasho
--------------------
pesho:
ala bala portokala
ala bala oranjada
####################
sasho:
khvbkhj buiyigv ,kjbj
khvbkhjyuvfuyv ,khvljh
Work items:
--------------------
ID :0
Title :newbug222222
Status :Active
Severity :Major
Priority :High
####################
ID :2
Title :newfeedback123321
Status :New
Rating 5
####################
ID :1
Title :newstory123321
Status :Not done
Size :Small
Priority: Medium
Assign work item: 0 newbug222222 to member: sasho!
Work item id already assigned to: sasho 
There is not a single work item assigned to pesho!
Change priority for ID: 0 newbug222222 to: High!
Rating cannot be less than 0 and more than 10.
Change severity for ID: 0 newbug222222 to: Major!
Change size for ID: 1 newstory123321 to: Large!
Change status for ID: 1 newstory123321 to: Done!
Work items:
--------------------
ID :0
Title :newbug222222
Status :Active
Severity :Major
Priority :High
Work items:
--------------------
ID :0
Title :newbug222222
Status :Active
Severity :Major
Priority :High
Work items:
--------------------
ID :1
Title :newstory123321
Status :Done
Size :Large
Priority: Medium
Work items:
--------------------
ID :1
Title :newstory123321
Status :Done
Size :Large
Priority: Medium
Work items:
--------------------
ID :2
Title :newfeedback123321
Status :New
Rating 5
Work items:
--------------------
ID :2
Title :newfeedback123321
Status :New
Rating 5
pesho activities are empty!
Team qgodki:
####################
Board with name board123

Members:
pesho
####################
sasho
Team qgodki:
Added Bug with #0 newbug222222
####################
Added Story with #1 newstory123321
####################
Added Feedback with #2 newfeedback123321
pesho activities are empty!
Team qgodki:
pesho added to team
####################
sasho added to team
####################
board123 added to team
Team qgodki:
####################
Member: pesho
####################
Member: sasho
```